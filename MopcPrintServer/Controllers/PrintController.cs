﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using FastReport.Export.OoXML;
using FastReport.Web;

namespace MopcPrintServer.Controllers
{
    public class PrinterController : Controller
    {
        // GET: Print
        public ActionResult Print(string id)
        {
            if (string.IsNullOrEmpty(id)) return HttpNotFound("No se recibió identificador de reporte");

            var identificador = id;
            var parametros = new List<KeyValuePair<string, object>>();
            var formato = string.Empty;

            var sepCount = id.Count(x => x == '|');
            if (sepCount > 0)
            {
                var arr = id.Split('|');
                identificador = arr[0];
                parametros = ExtraerParametros(arr[1]);

                if (sepCount > 1)
                {
                    formato = arr[2];
                }
            }

            if (!Guid.TryParse(identificador, out _)) return HttpNotFound("Nombre de archivo no válido");

            var report = new WebReport();
            var path = Server.MapPath($"~/App_Data/{identificador}");
            report.Report.Load(path);

            foreach (var parametro in parametros)
            {
                report.Report.SetParameterValue(parametro.Key, parametro.Value);
            }

            report.Width = Unit.Percentage(100);
            report.Height = Unit.Pixel(800);
            report.Report.Prepare();

            switch (formato)
            {
                case "PDF":
                    report.PrintPdf();
                    break;
                case "Excel":
                    // save file in stream
                    Stream stream = new MemoryStream();
                    report.Report.Export(new Excel2007Export(), stream);
                    stream.Position = 0;

                    // return stream in browser
                    return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "report.xlsx");
            }

            ViewBag.WebReport = report;

            return View();
        }

        private List<KeyValuePair<string, object>> ExtraerParametros(string str)
        {
            if (string.IsNullOrEmpty(str)) return new List<KeyValuePair<string, object>>(); //vacío
            var arr = str.Split(',');
            var res = new List<KeyValuePair<string, object>>();

            var splitCharacters = new[] { ">=", "<=", "=", ">", "<"};

            foreach (var val in arr)
            {
                var splitChars = splitCharacters.FirstOrDefault(character => val.Contains(character));
                if(string.IsNullOrEmpty(splitChars)) continue;

                var arr2 = val.Split(new[] { splitChars }, StringSplitOptions.None);

                var kvp = new KeyValuePair<string, object>(arr2[0].Trim(), arr2[1].Trim());
                res.Add(kvp);
            }
            return res;
        }
    }
}