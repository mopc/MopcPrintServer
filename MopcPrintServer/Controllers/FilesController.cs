﻿using System;
using System.Web.Mvc;
using MopcPrintServer.Models;
using MopcPrintServer.Repository;
using MopcPrintServer.ViewModels;

namespace MopcPrintServer.Controllers
{
    [Authorize]
    public class FilesController : Controller
    {
        private readonly IAplicacionRepository _aplicacionDb;
        private readonly IReporteRepository _reporteDb;

        public FilesController(
            IAplicacionRepository aplicacionDb,
            IReporteRepository reporteDb)
        {
            _aplicacionDb = aplicacionDb;
            _reporteDb = reporteDb;
        }
        // GET: Files
        public ActionResult Add(int? id)
        {
            if (!id.HasValue) return RedirectToAction("Index", "Application");
            var aplicacionViewModel = _aplicacionDb.BuscarAplicacionPorId(id.Value);
            if (aplicacionViewModel == null)
                return HttpNotFound();
            var model = new ReporteViewModel
            {
                AplicacionId = id.Value,
                AplicacionNombre = aplicacionViewModel.Nombre,
                AplicacionDescripcion = aplicacionViewModel.Descripcion
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(ReporteViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var nombreArchivo = model.File.FileName;
            if (!nombreArchivo.EndsWith(".frx"))
            {
                model.MensajeError = "El archivo debe ser un reporte de FastReport";
                return View(model);
            }

            var reporte = new Reporte
            {
                Nombre = model.Nombre,
                AplicacionId = model.AplicacionId,
                Identificador = Guid.NewGuid().ToString(),
                ParametrosPrueba = model.ParametrosPrueba
            };

            var ruta = Server.MapPath($"~/App_Data/{reporte.Identificador}");
            model.File.SaveAs(ruta);

            _reporteDb.Add(reporte);
            _reporteDb.SaveChanges();

            return RedirectToAction("Index", "Application");
        }

        public ActionResult Edit(int? id)
        {
            if (!id.HasValue) return RedirectToAction("Index", "Application");
            var model = _reporteDb.BuscarReportePorId(id.Value);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ReporteViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var modeloGuardado = _reporteDb.Get(model.Id);
            modeloGuardado.Nombre = model.Nombre;
            modeloGuardado.ParametrosPrueba = model.ParametrosPrueba;

            var nombreArchivo = model.File.FileName;
            if (!nombreArchivo.EndsWith(".frx"))
            {
                model.MensajeError = "El archivo debe ser un reporte de FastReport";
                return View(model);
            }

            var ruta = Server.MapPath($"~/App_Data/{model.Identificador}");
            model.File.SaveAs(ruta);

            _reporteDb.SaveChanges();

            return RedirectToAction("Index", "Application");
        }

        public FileResult DescargarReporte(string identificador)
        {
            var ruta = Server.MapPath($"~/App_Data/{identificador}");
            var fileBytes = System.IO.File.ReadAllBytes(ruta);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, $"{identificador}.frx");
        }
    }
}