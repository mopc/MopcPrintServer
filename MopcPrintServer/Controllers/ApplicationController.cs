﻿using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MopcPrintServer.Repository;
using MopcPrintServer.ViewModels;

namespace MopcPrintServer.Controllers
{
    [Authorize]
    public class ApplicationController : Controller
    {
        private readonly IAplicacionRepository _aplicacionRepository;
        public ApplicationController(IAplicacionRepository aplicacionRepository)
        {
            _aplicacionRepository = aplicacionRepository;
        }
        // GET: Reports
        public ActionResult Index()
        {
            var model = _aplicacionRepository.BuscarAplicacionesUsuario(User.Identity.GetUserId());
            return View(model);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(AplicacionViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            model.UsuarioId = User.Identity.GetUserId(); ;
            model.FechaRegistro = DateTime.Now;
            _aplicacionRepository.Agregar(model);

            return RedirectToAction("Index");
        }
    }
}