﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;
using FastReport.Export.Html;
using FastReport.Export.OoXML;
using FastReport.Export.Pdf;
using FastReport.Web;
using MopcPrintServer.Util;

namespace MopcPrintServer.Controllers.API
{
    /// <summary>
    /// Primera fase de desarrollo. Métodos que producen resultados
    /// instantáneos sin guardar reportes del lado servidor (segunda fase).
    /// </summary>


    [EnableCors(origins: "*", headers: "*", methods: "*")]
    //[AccesoApi]
    public class ExpressController : ApiController
    {
        /// <summary>
        /// El reporte express recibe el xml con la estructura del reporte en el request
        /// Este reporte espera un arreglo string con los siguientes 3 campos:
        /// [0]: Estructura: (Obligatorio), la estructura del reporte en formato xml
        /// [1]: Data: (Opcional), si no se incluye, se asume que la data está contenida en una base de datos a la que el servidor tiene acceso
        /// Si se incluye, obviará cualquier otra fuente de datos preconfigurada y le dará preferencia
        /// [2]: TipoRespuesta: (Opcional), Puede ser PDF o Excel. Si no se declara, se asume PDF.
        /// [3]: Parámetros: (Opcional)
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage ReporteSimple([FromBody]string[] requestData)
        {
            var estructura = "";
            DataSet data = null; 
            var tipoRespuesta = "";
            var parametros = new Dictionary<string, object>();

            var index = 0;
            foreach (var payload in requestData)
            {
                switch (index)
                {
                    case 0:
                        if (Guid.TryParse(payload, out var nombreArchivo))
                            estructura = File.ReadAllText(HostingEnvironment.MapPath($"~/App_Data/{nombreArchivo}") ?? throw new FileNotFoundException("Reporte no encontrado"));
                        else
                            estructura = Utilitario.ConvertirDeBase64AString(payload);
                        break;
                    case 1:
                        data = Utilitario.ExtraerData(payload, estructura);
                        break;
                    case 2:
                        tipoRespuesta = payload;
                        break;
                    case 3:
                        parametros = Utilitario.ExtraerParametros(payload);
                        break;
                }
                index++;
            }

            //Trabajar con la estructura
            var report = new WebReport
            {
                Report = { ReportResourceString = estructura }
            };

            //Trabajar con la data
            if (data != null)
                report.RegisterData(data);

            //Agregar parámetros
            foreach (var parametro in parametros)
            {
                report.Report.SetParameterValue(parametro.Key, parametro.Value);
            }

            report.Report.Prepare();

            //Trabajar con el tipo respuesta
            Stream stream = new MemoryStream();

            switch (tipoRespuesta)
            {
                case "Excel":
                    report.Report.Export(new Excel2007Export(), stream);
                    break;
                case "PDF":
                    report.Report.Export(new PDFExport(), stream);
                    break;
                case "HTML":
                    report.Report.Export(new HTMLExport(), stream);
                    break;
                default:
                    report.Report.Export(new PDFExport(), stream);
                    break;
            }

            stream.Position = 0;
            var result = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StreamContent(stream) };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }
    }
}
