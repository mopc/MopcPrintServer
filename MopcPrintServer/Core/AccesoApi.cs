﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Microsoft.AspNet.Identity.Owin;

namespace MopcPrintServer.Core
{
    public class AccesoApi : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);
            try
            {
                var code = actionContext.Request.Headers.GetValues("PrntServerAuthCode").FirstOrDefault();
                if (!Guid.TryParse(code, out var identificador)) throw new ArgumentException("Guid inválido");
                if (!AutorizarCodigo(identificador))
                {
                    throw new AuthenticationException("Código no autorizado");
                }
            }
            catch (InvalidOperationException e)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new {mensajeError = e.Message, excepcion = e});
            }
            catch (AuthenticationException e)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, new { mensajeError = e });
            }
            catch (Exception e)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, new { mensajeError = e });
            }
        }

        private static bool AutorizarCodigo(Guid code)
        {
            var context = HttpContext.Current.GetOwinContext();
            var userManager = context.GetUserManager<AppUserManager>();
            var usuario = userManager.Users.FirstOrDefault(x => x.Identificador == code);
            return usuario != null;
        }
    }
}