﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using MopcPrintServer.Models;

namespace MopcPrintServer.Core
{
    public class AppUserManager : UserManager<Usuario>
    {
        public AppUserManager(IUserStore<Usuario> store)
            : base(store)
        {
        }

        public static AppUserManager Create(
            IdentityFactoryOptions<AppUserManager> options, IOwinContext context)
        {
            var manager = new AppUserManager(
                new UserStore<Usuario>(context.Get<ApplicationDbContext>()));

            // Configuración opcional...

            return manager;
        }
    }

}