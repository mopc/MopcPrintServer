﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MopcPrintServer.Core;
using MopcPrintServer.Dto;
using MopcPrintServer.Models;
using MopcPrintServer.ViewModels;

namespace MopcPrintServer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Dashboard");
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Registro()
        {
            return View();
        }

        [Authorize]
        public ActionResult DashBoard()
        {
            var userManager = HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            var idUsuarioLogueado = User.Identity.GetUserId();
            ViewBag.Identificador = userManager.Users.FirstOrDefault(x => x.Id == idUsuarioLogueado)?.Identificador;
            return View();
        }

        public ActionResult IniciarSesion(LoginViewModel lvm)
        {
            if (!ModelState.IsValid)
            {
                return View("Login", lvm);
            }

            var credenciales = new Credenciales
            {
                Usuario = lvm.Usuario,
                Contrasena = lvm.Contrasena
            };

            var userManager = HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            var authManager = HttpContext.GetOwinContext().Authentication;

            var usuario = userManager.Find(credenciales.Usuario, credenciales.Contrasena);
            if (usuario == null) return RedirectToAction("Login");
            var ident = userManager.CreateIdentity(usuario, DefaultAuthenticationTypes.ApplicationCookie);
            authManager.SignIn(new AuthenticationProperties { IsPersistent = false }, ident);
            return RedirectToAction("Dashboard");
        }

        public ActionResult CerrarSesion()
        {
            var authManager = HttpContext.GetOwinContext().Authentication;
            authManager.SignOut();
            return RedirectToAction("Index");
        }

        public ActionResult CrearUsuario(RegistrarViewModel rvm)
        {
            if (!ModelState.IsValid)
            {
                return View("Registro", rvm);
            }

            var userManager = HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            var credenciales = new Credenciales
            {
                Usuario = rvm.Usuario,
                Contrasena = rvm.Contrasena
            };
            var usuario = userManager.FindByEmail(credenciales.Usuario);
            if (usuario != null) return RedirectToAction("Login");
            userManager.Create(new Usuario { UserName = credenciales.Usuario, Email = credenciales.Usuario}, credenciales.Contrasena);
            var lvm = new LoginViewModel
            {
                Usuario = credenciales.Usuario,
                Contrasena = credenciales.Contrasena
            };
            return IniciarSesion(lvm);
        }
    }
}