﻿using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;

namespace MopcPrintServer
{
    public static class IoCFactoryConfig
    {
        public static void Build()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinderProvider();

            builder.RegisterType<ApplicationDbContext>();
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(x => x.Namespace != null && x.Name.EndsWith("Repository") && x.Namespace.EndsWith("Repository"))
                .As(x => x.GetInterfaces().FirstOrDefault(i => i.Name == "I" + x.Name));

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}