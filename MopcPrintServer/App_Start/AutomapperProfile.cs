﻿using AutoMapper;
using MopcPrintServer.Models;
using MopcPrintServer.ViewModels;

namespace MopcPrintServer
{
    public class AutoMapperProfile : Profile
    {
        public static void Initialize()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<AutoMapperProfile>();
            });
            Mapper.Configuration.AssertConfigurationIsValid();
        }

        public AutoMapperProfile()
        {
            //Aquí van los mappings manuales. Ejemplo:
            CreateMap<Aplicacion, AplicacionViewModel>().ReverseMap();
        }
    }
}