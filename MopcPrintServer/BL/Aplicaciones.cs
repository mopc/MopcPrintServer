﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MopcPrintServer.Models;

namespace MopcPrintServer.BL
{
    public class Aplicaciones
    {
        private readonly ApplicationDbContext _db;
        public Aplicaciones()
        {
            _db = new ApplicationDbContext();
        }

        public IEnumerable<Aplicacion> BuscarAplicaciones(string usuarioId)
        {
            return _db.Aplicaciones.Where(x => x.UsuarioId == usuarioId)
                .Include(x => x.Reportes);
        }
    }
}