﻿namespace MopcPrintServer.Dto
{
    public class Credenciales
    {
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
        public string Confirmacion { get; set; }
    }
}