﻿using System.ComponentModel.DataAnnotations;

namespace MopcPrintServer.ViewModels
{
    public class RegistrarViewModel
    {
        [Required(ErrorMessage = "El nombre de usuario es requerido")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Debe introducir una dirección de correo electrónico válida")]
        [MinLength(5, ErrorMessage = "El usuario debe tener como mínimo 5 caracteres")]
        public string Usuario { get; set; }

        [MinLength(8, ErrorMessage = "La contraseña deber tener al menos 8 caracteres")]
        [MaxLength(50, ErrorMessage = "La contraseña no debe ser mayor a 50 caracteres")]
        [Required(ErrorMessage = "La contraseña es requerida")]
        public string Contrasena { get; set; }

        [Compare("Contrasena", ErrorMessage = "Las contraseñas no coinciden")]
        public string Confirmacion { get; set; }
    }
}