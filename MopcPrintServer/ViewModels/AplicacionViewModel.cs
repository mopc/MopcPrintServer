﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MopcPrintServer.ViewModels
{
    public class AplicacionViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El nombre de la aplicación es requerido")]
        [MaxLength(250, ErrorMessage = "El largo máximo permitido es de 250 caracteres")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "La descripción es requerida")]
        [MaxLength(1024, ErrorMessage = "El largo máximo permitido es de 1024 caracteres")]
        public string Descripcion { get; set; }

        public string UsuarioId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public IList<ReporteViewModel> Reportes { get; set; }
    }
}