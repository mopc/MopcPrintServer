﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MopcPrintServer.ViewModels
{
    public class ReporteViewModel
    {
        public int Id { get; set; }
        public int AplicacionId { get; set; }
        public string AplicacionNombre { get; set; }
        public string AplicacionDescripcion { get; set; }

        [Required(ErrorMessage = "El nombre del reporte es requerido")]
        public string Nombre { get; set; }

        [Display(Name = "Archivo FastReport")]
        [Required(ErrorMessage = "El archivo de reporte es requerido")]
        public HttpPostedFileBase File { get; set; }

        public string MensajeError { get; set; }
        public string Identificador { get; set; }

        [Display(Name = "Parámetros de Prueba")]
        public string ParametrosPrueba { get; set; }

    }
}