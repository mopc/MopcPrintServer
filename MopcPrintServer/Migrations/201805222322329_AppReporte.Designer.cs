// <auto-generated />
namespace MopcPrintServer.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AppReporte : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AppReporte));
        
        string IMigrationMetadata.Id
        {
            get { return "201805222322329_AppReporte"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
