namespace MopcPrintServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActualizarAplicacion : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Aplicaciones", "UsuarioId", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Aplicaciones", "UsuarioId", c => c.Int(nullable: false));
        }
    }
}
