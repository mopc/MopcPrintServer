namespace MopcPrintServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class parametrosprueba : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reportes", "ParametrosPrueba", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reportes", "ParametrosPrueba");
        }
    }
}
