namespace MopcPrintServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AppReporte : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Aplicaciones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UsuarioId = c.Int(nullable: false),
                        Nombre = c.String(maxLength: 250),
                        Descripcion = c.String(maxLength: 1024),
                        FechaRegistro = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Reportes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AplicacionId = c.Int(nullable: false),
                        Nombre = c.String(maxLength: 100),
                        Identificador = c.String(maxLength: 50),
                        Ruta = c.String(maxLength: 1024),
                        NombreArchivo = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Aplicaciones", t => t.AplicacionId, cascadeDelete: true)
                .Index(t => t.AplicacionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reportes", "AplicacionId", "dbo.Aplicaciones");
            DropIndex("dbo.Reportes", new[] { "AplicacionId" });
            DropTable("dbo.Reportes");
            DropTable("dbo.Aplicaciones");
        }
    }
}
