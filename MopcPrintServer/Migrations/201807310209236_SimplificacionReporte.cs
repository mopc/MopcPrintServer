namespace MopcPrintServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SimplificacionReporte : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Reportes", "Ruta");
            DropColumn("dbo.Reportes", "NombreArchivo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reportes", "NombreArchivo", c => c.String(maxLength: 100));
            AddColumn("dbo.Reportes", "Ruta", c => c.String(maxLength: 1024));
        }
    }
}
