﻿using System;
using System.Collections.Generic;

namespace MopcPrintServer.Models
{
    public class Aplicacion
    {
        public int Id { get; set; }
        public string UsuarioId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaRegistro { get; set; } = DateTime.Now;
        public IList<Reporte> Reportes { get; set; }
    }
}