﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MopcPrintServer.Models
{
    public class Usuario : IdentityUser
    {
        public Guid Identificador { get; set; } = Guid.NewGuid();
    }
}