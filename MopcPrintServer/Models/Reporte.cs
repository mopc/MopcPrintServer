﻿using System;

namespace MopcPrintServer.Models
{
    public class Reporte
    {
        public int Id { get; set; }
        public int AplicacionId { get; set; }
        public Aplicacion Aplicacion { get; set; }
        public string Nombre { get; set; }
        public string Identificador { get; set; } = Guid.NewGuid().ToString();
        public string ParametrosPrueba { get; set; }
    }
}