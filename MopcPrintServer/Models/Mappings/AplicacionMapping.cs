﻿using System.Data.Entity.ModelConfiguration;

namespace MopcPrintServer.Models.Mappings
{
    public class AplicacionMapping : EntityTypeConfiguration<Aplicacion>
    {
        public AplicacionMapping()
        {
            ToTable("Aplicaciones");
            Property(x => x.UsuarioId)
                .HasMaxLength(50);
            Property(x => x.Nombre)
                .HasMaxLength(250);
            Property(x => x.Descripcion)
                .HasMaxLength(1024);
        }

    }
}