﻿using System.Data.Entity.ModelConfiguration;

namespace MopcPrintServer.Models.Mappings
{
    public class ReporteMapping : EntityTypeConfiguration<Reporte>
    {
        public ReporteMapping()
        {
            ToTable("Reportes");

            Property(x => x.Identificador)
                .HasMaxLength(50);
            Property(x => x.Nombre)
                .HasMaxLength(100);
            Property(x => x.ParametrosPrueba)
                .HasMaxLength(1024);
        }
    }
}