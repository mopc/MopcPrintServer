using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using MopcPrintServer.Models;
using MopcPrintServer.Models.Mappings;

namespace MopcPrintServer
{
    public class ApplicationDbContext : IdentityDbContext<Usuario>
    {
        public ApplicationDbContext()
            : base("name=ApplicationDbContext")
        {
        }

        public virtual DbSet<Aplicacion> Aplicaciones { get; set; }
        public virtual DbSet<Reporte> Reportes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AplicacionMapping());
            modelBuilder.Configurations.Add(new ReporteMapping());
            base.OnModelCreating(modelBuilder);
        }
    }

}