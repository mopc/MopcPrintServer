﻿using System.Data.Entity;
using System.Linq;
using AutoMapper;
using MopcPrintServer.Models;
using MopcPrintServer.ViewModels;

namespace MopcPrintServer.Repository
{
    public interface IReporteRepository : IRepository<Reporte>
    {
        ReporteViewModel BuscarReportePorId(int id);
    }

    public class ReporteRepository : Repository<Reporte>, IReporteRepository
    {
        public ReporteRepository(ApplicationDbContext context) : base(context)
        {}

        public ReporteViewModel BuscarReportePorId(int id)
        {
            var reporte = Context.Reportes
                .Include(x=>x.Aplicacion)
                .FirstOrDefault(x => x.Id == id);
            if (reporte == null) return null;
            var model = new ReporteViewModel();
            Mapper.Map(reporte, model);
            return model;
        }
    }
}