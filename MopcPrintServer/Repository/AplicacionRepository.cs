﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using MopcPrintServer.Models;
using MopcPrintServer.ViewModels;

namespace MopcPrintServer.Repository
{
    public interface IAplicacionRepository : IRepository<Aplicacion>
    {
        AplicacionViewModel BuscarAplicacionPorId(int id);
        IEnumerable<AplicacionViewModel> BuscarAplicacionesUsuario(string usuarioId);
        void Agregar(AplicacionViewModel model);
    }

    public class AplicacionRepository : Repository<Aplicacion>, IAplicacionRepository
    {
        private readonly ApplicationDbContext _db;
        public AplicacionRepository(ApplicationDbContext context) : base(context)
        {
            _db = context;
        }

        public AplicacionViewModel BuscarAplicacionPorId(int id)
        {
            var app = Get(id);
            if (app == null) return null;
            var model = new AplicacionViewModel();
            Mapper.Map(app, model);
            return model;
        }

        public IEnumerable<AplicacionViewModel> BuscarAplicacionesUsuario(string usuarioId)
        {
            var aplicaciones = _db.Aplicaciones.Where(x => x.UsuarioId == usuarioId)
                .Include(x => x.Reportes);
            var aplicacionViewModel = new List<AplicacionViewModel>();
            Mapper.Map(aplicaciones, aplicacionViewModel);
            return aplicacionViewModel;
        }

        public void Agregar(AplicacionViewModel model)
        {
            var aplicacion = new Aplicacion();
            Mapper.Map(model, aplicacion);
            _db.Aplicaciones.Add(aplicacion);
            _db.SaveChanges();
        }
    }
}