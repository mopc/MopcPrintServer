﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Xml;
using Newtonsoft.Json;

namespace MopcPrintServer.Util
{
    public static class Utilitario
    {
        public static string ConvertirDeBase64AString(string b64Str)
        {
            var data = Convert.FromBase64String(b64Str);
            var decodedString = Encoding.Default.GetString(data);
            return decodedString;
        }

        public static Dictionary<string, object> ExtraerParametros(string b64Str)
        {
            var str = ConvertirDeBase64AString(b64Str);
            var r = JsonConvert.DeserializeObject<List<KeyValuePair<string, object>>>(str);
            return r.ToDictionary(x => x.Key, x => x.Value);
        }

        public static DataSet ExtraerData(string b64Str, string estructura)
        {
            if (string.IsNullOrEmpty(b64Str)) return null;
            var str = ConvertirDeBase64AString(b64Str);
            var ds = new DataSet();
            var tbl = new DataTable();

            var xdc = new XmlDocument();
            xdc.LoadXml(estructura);
            var node = xdc.SelectSingleNode(".//TableDataSource");
            var nombreTabla = node.Attributes["Alias"]?.Value ?? node.Attributes["Name"].Value;
            var columnaNodes = node.SelectNodes("./Column");

            tbl.TableName = nombreTabla;

            foreach (XmlNode cn in columnaNodes)
            {
                var nombre = cn.Attributes["Name"].Value;
                var tipo = cn.Attributes["DataType"].Value;
                tbl.Columns.Add(nombre, Type.GetType(tipo));
            }

            var data = JsonConvert.DeserializeObject<List<ExpandoObject>>(str);

            foreach (var d in data)
            {
                var valores = (IDictionary<string, object>)d;
                var arrVal = valores.Select(x => x.Value).ToArray();
                tbl.Rows.Add(arrVal);
            }

            ds.Tables.Add(tbl);
            return ds;
        }
    }
}